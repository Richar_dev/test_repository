CREATE TABLE personas(
	id_persona SERIAL,
	nombre VARCHAR(50),
	apellido VARCHAR(50),
	edad SMALLINT,
	PRIMARY KEY(id_persona)
);